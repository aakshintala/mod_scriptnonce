/* Include the required headers from httpd */
#include "httpd.h"
#include "http_core.h"
#include "http_protocol.h"
#include "http_request.h"

#include "apr_strings.h"
#include "apr_network_io.h"
#include "apr_md5.h"
//#include "apr_sha1.h"
//#include "apr_hash.h"
#include "apr_base64.h"
#include "apr_dbd.h"
#include <apr_file_info.h>
#include <apr_file_io.h>
#include <apr_tables.h>
#include "util_script.h"

#include "http_config.h"
#include "apr_buckets.h"
#include "apr_general.h"
#include "apr_lib.h"
#include "util_filter.h"
#include "apr_tables.h"
#include "http_log.h"

#define NONCE_MAX_STRING_LEN 50
#define SALT_LEN 31

/* Define prototypes of our functions in this module */
static void register_script_nonce_hooks(apr_pool_t *pool);
static apr_status_t script_nonce_csp_header_filter(ap_filter_t* f, apr_bucket_brigade* bb);
static int script_nonce_csp_header_handler(request_rec *r);
static void ap_csp_headers_insert_output_filter(request_rec *r);
static const char *set_key_pattern(cmd_parms *cmd, void *cfg, const char *t);
static void *allocateptm(apr_pool_t *p, server_rec *s);

typedef struct {
	char * pattern;
	apr_size_t patlen;
} ptm;

// ptm * patterntomatch = NULL;

static const command_rec nonce_cmds[] =
{
   AP_INIT_TAKE1("PatternToReplace", set_key_pattern, NULL, RSRC_CONF,
     "Set the pattern to replace with the nonce"),
    {NULL}
};

/* Define our module as an entity and assign a function for registering hooks  */

module AP_MODULE_DECLARE_DATA script_nonce_module =
{
	STANDARD20_MODULE_STUFF,
	NULL,            			// Per-directory configuration handler
	NULL,            			// Merge handler for per-directory configurations
	allocateptm,            	// Per-server configuration handler
	NULL,            			// Merge handler for per-server configurations
	nonce_cmds,            		// Any directives we may have for httpd
	register_script_nonce_hooks // Our hook registering function
};

static void *allocateptm(apr_pool_t *p, server_rec *s)
{
	ptm *patterntomatch = apr_pcalloc(p, sizeof(ptm));
	patterntomatch->pattern = "NULL";
	patterntomatch->patlen = 0;
	return patterntomatch;
}

static const char *set_key_pattern(cmd_parms *cmd, void *cfg, const char *value)
{
	// ap_log_error(APLOG_MARK, APLOG_CRIT, 0, cmd->server, "getting the key!0");
	ptm* patterntomatch = (ptm *)ap_get_module_config(cmd->server->module_config,&script_nonce_module);
	//patterntomatch = (ptm *)apr_pcalloc(cmd->pool, sizeof(ptm));
	// ap_log_error(APLOG_MARK, APLOG_CRIT, 0, cmd->server, "getting the key!1");
	patterntomatch->pattern = (char*)value;
	// ap_log_error(APLOG_MARK, APLOG_CRIT, 0, cmd->server, "getting the key!2");
	patterntomatch->patlen = sizeof(value);
	ap_log_error(APLOG_MARK, APLOG_CRIT, 0, cmd->server, "getting the key!3");
	return NULL;
}


/* register_hooks: Adds a hook to the httpd process */
static void register_script_nonce_hooks(apr_pool_t *pool) 
{
	/* Hook the request handler */
	ap_register_output_filter("CSP_SCRIPT_NONCE", script_nonce_csp_header_filter, NULL, AP_FTYPE_RESOURCE); //AP_FTYPE_CONTENT_SET);
	ap_hook_insert_filter(ap_csp_headers_insert_output_filter, NULL, NULL, APR_HOOK_MIDDLE);
}

// the following function is made up of code borrowed from code found at 
//http://datapile.coffeecrew.org/2010/11/02/programming-with-the-apr-using-md5/
void get_random_nonce(char* Nonce) {    

  apr_status_t rv;
  char salt1[SALT_LEN];
  char salt2[SALT_LEN];
  unsigned char md5DigestSalted[NONCE_MAX_STRING_LEN];
  unsigned char b64_md5digest[apr_base64_encode_len(NONCE_MAX_STRING_LEN)];

  rv = apr_generate_random_bytes(salt1, SALT_LEN - 1);
  salt1[SALT_LEN -1] = '\0';
  rv = apr_generate_random_bytes(salt2, SALT_LEN - 1);
  salt2[SALT_LEN -1] = '\0';

  apr_md5_encode(salt1,
         salt2, 
         md5DigestSalted, 
         sizeof(md5DigestSalted));

  
  apr_base64_encode(b64_md5digest,
	md5DigestSalted,
	sizeof(md5DigestSalted));	

  apr_cpystrn(Nonce, b64_md5digest, NONCE_MAX_STRING_LEN);
  return;
}

static apr_status_t script_nonce_csp_header_filter(ap_filter_t* f, apr_bucket_brigade* bb) {
	/* read a chunk of data, process it, pass it to the next filter */
	const char *csp_header_webkit = NULL;
	const char *csp_header_chrome = NULL;
	const char *csp_header_firefox = NULL;
	char *script_nonce = NULL;
	char *script_nonce_chrome = NULL;
	char *script_nonce_firefox = NULL;
	apr_size_t nonceLen = 0;
	char nonce[apr_base64_encode_len(NONCE_MAX_STRING_LEN)];// = "hey there";
	memset(nonce, 0, apr_base64_encode_len(NONCE_MAX_STRING_LEN));

	apr_bucket *b = NULL;
	ap_log_error(APLOG_MARK, APLOG_CRIT, 0, f->r->server, "before pattern retrieval");
	ptm* patterntomatch = (ptm *)ap_get_module_config(f->r->server->module_config,&script_nonce_module);
	char *pattern = patterntomatch->pattern; //"hellohello";
	ap_log_error(APLOG_MARK, APLOG_CRIT, 0, f->r->server, "after pattern retrieval");

	get_random_nonce(nonce);
	nonceLen = strlen(nonce);

	csp_header_webkit = apr_table_get(f->r->headers_out, "X-WebKit-CSP");
	csp_header_chrome = apr_table_get(f->r->headers_out, "Content-Security-Policy");
	csp_header_firefox = apr_table_get(f->r->headers_out, "X-Content-Security-Policy");

	if (!csp_header_webkit) {
		script_nonce = apr_psprintf(f->r->pool, "script-nonce %s", nonce);
		apr_table_addn(f->r->headers_out, "X-WebKit-CSP", script_nonce);
	} else {
		script_nonce = apr_psprintf(f->r->pool, "%s; script-nonce %s", csp_header_webkit, nonce);
		apr_table_setn(f->r->headers_out, "X-WebKit-CSP", script_nonce);
	} 

	if (!csp_header_chrome) {
		script_nonce_chrome = apr_psprintf(f->r->pool, "script-nonce %s", nonce);
		apr_table_addn(f->r->headers_out, "Content-Security-Policy", script_nonce_chrome);
	} else {
		script_nonce_chrome = apr_psprintf(f->r->pool, "%s; script-nonce %s", csp_header_chrome, nonce);
		apr_table_setn(f->r->headers_out, "Content-Security-Policy", script_nonce_chrome);
	} 

	if (!csp_header_firefox) {
		script_nonce_firefox = apr_psprintf(f->r->pool, "script-nonce %s", nonce);
		apr_table_addn(f->r->headers_out, "X-Content-Security-Policy", script_nonce_firefox);
	} else {
		script_nonce_firefox = apr_psprintf(f->r->pool, "%s; script-nonce %s", csp_header_firefox, nonce);
		apr_table_setn(f->r->headers_out, "X-Content-Security-Policy", script_nonce_firefox);
	}

#if 1
	for ( b = APR_BRIGADE_FIRST(bb); b != APR_BRIGADE_SENTINEL(bb); b = APR_BUCKET_NEXT(b) ) {
    const char* buf ;
    size_t bytes ;

		if ( APR_BUCKET_IS_EOS(b) ) {
      /* end of input file - insert footer if any */
      //if ( ctxt->foot && ! (ctxt->state & TXT_FOOT ) ) {
			//ctxt->state |= TXT_FOOT ;
			//APR_BUCKET_INSERT_BEFORE(b, ctxt->foot);
			continue;
    } else if (apr_bucket_read(b, &buf, &bytes, APR_BLOCK_READ) == APR_SUCCESS)  {
			/* We have a bucket full of text.  Just escape it where necessary */
			char *bufline = apr_psprintf(f->r->pool, "buffer read = %s", buf);
			ap_log_error(APLOG_MARK, APLOG_CRIT, 0, f->r->server, "NETWORKSECURITY: %s", bufline);
      size_t count = 0;
      const char* p = buf;
      ap_log_error(APLOG_MARK, APLOG_CRIT, 0, f->r->server, "00\n");
      if(pattern == NULL)
      	ap_log_error(APLOG_MARK, APLOG_CRIT, 0, f->r->server, "Pattern is null?\n");
			size_t patternLen = strlen(pattern);
			ap_log_error(APLOG_MARK, APLOG_CRIT, 0, f->r->server, "01%s\n", pattern);

			while ( count < bytes ) {
				ap_log_error(APLOG_MARK, APLOG_CRIT, 0, f->r->server, "0\n");
				char *szp = strstr(p, pattern) ;
				ap_log_error(APLOG_MARK, APLOG_CRIT, 0, f->r->server, "1\n");
				size_t sz = szp - p;
				ap_log_error(APLOG_MARK, APLOG_CRIT, 0, f->r->server, "2\n");
				count += sz ;

				if ( count < bytes ) {
					char *line = apr_psprintf(f->r->pool, "count = %lu, size = %lu, patternLen = %lu, nonceLen = %lu, location = %s", count, sz, patternLen, nonceLen, szp);
					ap_log_error(APLOG_MARK, APLOG_CRIT, 0, f->r->server, "NETWORKSECURITY: %s", line);
					apr_bucket_split(b, sz) ;	//Split off before buffer
	  				b = APR_BUCKET_NEXT(b) ;	//Skip over before buffer
	  				APR_BUCKET_INSERT_BEFORE(b, apr_bucket_transient_create(nonce, nonceLen, f->r->connection->bucket_alloc)); //Insert the replacement
	  				apr_bucket_split(b, patternLen);	//Split off the char to remove
	  				APR_BUCKET_REMOVE(b) ;	 //... and remove it
	  				b = APR_BUCKET_NEXT(b) ;	//Move cursor on to what-remains so that it stays in sequence with our main loop
	  				count += patternLen ;
					p += sz + patternLen ;
				}
			}
		}
	}
#endif

	/* remove ourselves from the filter chain */
	ap_remove_output_filter(f);

	/* send the data up the stack */
	return ap_pass_brigade(f->next, bb);
}

static void ap_csp_headers_insert_output_filter(request_rec *r)
{
	ap_add_output_filter("CSP_SCRIPT_NONCE", NULL, r, r->connection);
}


